
package edu.prakriti.socialtime.controller;
import edu.prakriti.socialtime.database.DatabaseConnection;
import edu.prakriti.socialtime.model.User;
import edu.prakriti.socialtime.utils.ProjectUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author PrAkRiTi
 */
public class UserController {
    private Connection con;
    private PreparedStatement prst;
    private ResultSet rs; 
    
    public UserController(){
        if(con==null){
            con= DatabaseConnection.getConnection();
        }
    }
         public User authenticate(User userobj){
            User currentUser= null;
            String query= "select * from user where username = ? and password= ?";
        try{
         //ViewUser.user_id= ResultSet.getInt("user_id");
         prst= con.prepareStatement(query);
         prst.setString(1, userobj.getUsername());
         prst.setString(2, userobj.getPassword());
         
         rs= prst.executeQuery();
         
         while(rs.next()){
             currentUser= new User();
             currentUser.setUserId(rs.getInt("user_id"));
             currentUser.setUsername(rs.getString("username"));
         }
         } catch(SQLException ex){
             Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
         }
        return currentUser;
         }
         
    public int register(User userobj){
        int isUserRegistered= 0;
        String query= "Insert into User(username, password, fullname, "
                + "email, address, phone_no, gender) values(?,?,?,?,?,?,?)";
    try{
        prst= con.prepareStatement(query);
            prst.setString(1, userobj.getUsername());
            prst.setString(2,userobj.getPassword());
            prst.setString(3, userobj.getFullname());
            prst.setString(4, userobj.getEmail());
            prst.setString(5, userobj.getAddress());
            prst.setString(6, userobj.getPhoneNo());
            prst.setString(7, userobj.getGender());
            isUserRegistered= prst.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE,null,e);
    }
    return isUserRegistered;
    }
    public DefaultTableModel buildTableData(int u_id) throws SQLException{
        DefaultTableModel data = ProjectUtils.buildDataTable(getSpecificMember(u_id));
        return data;
    }
    
    public ResultSet getAllMembers(){
        
        ResultSet rs = null;
        try{
            String query = "SELECT * FROM user where user_id=  ";
            prst = con.prepareStatement(query);
            rs = prst.executeQuery();
        }catch(SQLException e){
        }
        return rs;
    }
    public ResultSet getSpecificMember(int u_id){
        ResultSet rs = null;
        try{
            String query = "SELECT * FROM user where user_id="+u_id ;
            prst = con.prepareStatement(query);
            rs = prst.executeQuery();
        }catch(SQLException e){
        }
        return rs;
    }
    
     public ResultSet getInvitation(int u_id){
        ResultSet rs = null;
        try{
            String query = "SELECT * FROM event where user_id="+u_id ;
            prst = con.prepareStatement(query);
            rs = prst.executeQuery();
        }catch(SQLException e){
        }
        return rs;
    }
    }


