/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.prakriti.socialtime.controller;

import edu.prakriti.socialtime.database.DatabaseConnection;
import edu.prakriti.socialtime.model.AvailableTimeSlots;
import edu.prakriti.socialtime.model.Event;
import edu.prakriti.socialtime.model.UnavailableTimeSlot;
import edu.prakriti.socialtime.model.User;
import edu.prakriti.socialtime.utils.InvitationUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PrAkRiTi
 */
public class EventController {
    private Connection conn= null;
    private PreparedStatement prst;
    private ResultSet rs;
   
    public EventController(){
        if(conn==null){
            conn= DatabaseConnection.getConnection();
        }
    }
    public int createEvent(Event event, User user)throws SQLException{
         int iseventCreated= 0;
        String Eventquery= "insert into event(user_id, event_name, duration,"+ " location, Description) Values(?,?,?,?,?)";
        try{
        prst= conn.prepareStatement(Eventquery);
        prst.setInt(1, user.getUserId());
        prst.setString(2, event.getEventName());
        prst.setString(3, event.getDuration());
        prst.setString(4, event.getLocation());
        prst.setString(5,event.getDescription());
        
            iseventCreated= prst.executeUpdate();
        }catch(SQLException e){  
}
        return iseventCreated;
    }
    
    public int AvailableSlot(AvailableTimeSlots avTime, User user) throws SQLException{
        int isAvTimeEntered= 0;
        String availableTime= "insert into available_time_slots(av_time, av_day, "
                + "av_date, user_id) values(?,?,?,?)";
        
        prst= conn.prepareStatement(availableTime);
        prst.setString(1, avTime.getAvTime());
        prst.setString(2, avTime.getAvDay());
        prst.setString(3, avTime.getAvDate());
        prst.setInt(4, user.getUserId());
        
        isAvTimeEntered= prst.executeUpdate();
        return isAvTimeEntered;
    }
    
    public int UnavailableSlot(UnavailableTimeSlot unTime, User user) throws SQLException{
        int isUnTimeEntered= 0;
        String unavailableTime= "insert into unavailable_time_slot(user_id, un_time,"+ " un_day, un_date) values(?,?,?,?)";
        prst= conn.prepareStatement(unavailableTime);
        prst.setInt(1, user.getUserId());
        prst.setString(2, unTime.getUnTime());
        prst.setString(3, unTime.getUnDay());
        prst.setString(4, unTime.getUnDate());
        
        isUnTimeEntered= prst.executeUpdate();
        return isUnTimeEntered;
    }
    
      public int InviteUser(User user){
        int invited=0;
        String invitation = "UPDATE user SET event_request = ? WHERE user_id"
                + " <>'"+user.getUserId()+"'";
        try {
            prst=conn.prepareStatement(invitation);
            prst.setInt(1, user.getUserId());
            
            invited = prst.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return invited;
    }
      public int getInvitation(){
        ResultSet rs;
        String query = "Select * from user";
        int value=0;
        try {
            prst = conn.prepareStatement(query);
            rs = prst.executeQuery();
            while(rs.next()){
                value = rs.getInt("event_request");
            }
        } catch (SQLException e) {
        }
        return value;
    }
      public int eventAttendees(User usr){
        int attendees=0;
        ResultSet event = null;
        String sql = "insert into attendee value(?,?)";
        try {
            prst = conn.prepareStatement(sql);
            prst.setInt(1, usr.getUserId());
            prst.setInt(2, event.getInt("event_id"));
            attendees = prst.executeUpdate();
            
        } catch (SQLException e) {
        }
        
        return attendees;
    }
      public int getAttendeeEvent(User user){
        int event_id=0;
        int user_id=0;
        String sql = "select * from event where user_id=?";
        try {
            String query = "SELECT * FROM user where user_id=?";
            prst = conn.prepareStatement(query);
            prst.setInt(1, user.getUserId());
            rs = prst.executeQuery();
            
            while(rs.next()){
                user_id = rs.getInt("event_request");
            }
            String query2 = "select * from event where user_id=?";
            prst = conn.prepareStatement(query2);
            prst.setInt(1, user_id);
            rs = prst.executeQuery();
            while(rs.next()){
                event_id = rs.getInt("event_id");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return event_id;
    
      }
      public ResultSet getEventId(User user){
        String sql="Select * from event where user_id='"+user.getUserId()+"'";
        
        try {
            prst = conn.prepareStatement(sql);
            rs = prst.executeQuery();
        } catch (SQLException e) {
        }
        return rs;
    }
    public int delRequest(User user){
        int event_request=0;
        String sql = "UPDATE user SET event_request = ? WHERE user_id = '"+user.getUserId()+"'";
        try {
            prst=conn.prepareStatement(sql);
            prst.setInt(1, event_request);
            
            event_request = prst.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return event_request;
    }
     public DefaultTableModel buildTableData(User user) throws SQLException{
        DefaultTableModel data = InvitationUtil.buildDataTable(getAllEvent(user));
        return data;
    }
      public ResultSet getAllEvent(User user){
        int value = 0;
        ResultSet rs = null;
        try{
            String query = "SELECT * FROM user where user_id=?";
            prst = conn.prepareStatement(query);
            prst.setInt(1, user.getUserId());
            rs = prst.executeQuery();
            
            while(rs.next()){
                value = rs.getInt("event_request");
            }
            String query2 = "select * from event where user_id=?";
            prst = conn.prepareStatement(query2);
            prst.setInt(1, value);
            rs = prst.executeQuery();
        }catch(Exception e){
            e.printStackTrace();
        }
        return rs;
    }
}

        
    
    

