

package edu.prakriti.socialtime.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author PrAkRiTi
 */
public class DatabaseConnection {
    private static Connection con= null;
    public static Connection getConnection(){
        if(con==null){
            connectToDb();
        }
        return con;
    }
private static void connectToDb(){
    try{
        Class.forName("com.mysql.jdbc.Driver");
    } catch(ClassNotFoundException ex){
        Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
    }
    try{
        con= DriverManager.getConnection("jdbc:mysql://localhost:3306/event_controller", "root", "");
    }catch(SQLException ex){
        JOptionPane.showMessageDialog(null, "Database connection error! Please check database settings");
        Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
}
}