/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.prakriti.socialtime.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author PrAkRiTi
 */
   public class InvitationUtil {
     public static DefaultTableModel buildDataTable(ResultSet ReS) throws SQLException {
        ResultSetMetaData meta_data = ReS.getMetaData();
        int columnCount = meta_data.getColumnCount();
        Object[] columnNames = new Object[columnCount];
        int index = 0;
        for (int column = 1; column <= columnCount; column++) {
            columnNames[index] = meta_data.getColumnName(column).toUpperCase();
            index++;
        }
        ReS.last();
        Object[][] data = new Object[ReS.getRow()][columnCount];
        ReS.beforeFirst();
        while (ReS.next()) {
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                data[ReS.getRow() - 1][columnIndex - 1] = ReS.getObject(columnIndex);
            }
        }
        return new DefaultTableModel(data, columnNames);
    }
    
    
    
    
    
}
