/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.prakriti.socialtime.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PrAkRiTi
 */
@Entity
@Table(name = "attendee", catalog = "event_controller", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Attendee.findAll", query = "SELECT a FROM Attendee a"),
    @NamedQuery(name = "Attendee.findByAttendeeId", query = "SELECT a FROM Attendee a WHERE a.attendeeId = :attendeeId"),
    @NamedQuery(name = "Attendee.findByUserId", query = "SELECT a FROM Attendee a WHERE a.userId = :userId"),
    @NamedQuery(name = "Attendee.findByEventId", query = "SELECT a FROM Attendee a WHERE a.eventId = :eventId")})
public class Attendee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "attendee_id")
    private Integer attendeeId;
    @Basic(optional = false)
    @Column(name = "user_id")
    private int userId;
    @Basic(optional = false)
    @Column(name = "event_id")
    private int eventId;

    public Attendee() {
    }

    public Attendee(Integer attendeeId) {
        this.attendeeId = attendeeId;
    }

    public Attendee(Integer attendeeId, int userId, int eventId) {
        this.attendeeId = attendeeId;
        this.userId = userId;
        this.eventId = eventId;
    }

    public Integer getAttendeeId() {
        return attendeeId;
    }

    public void setAttendeeId(Integer attendeeId) {
        this.attendeeId = attendeeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attendeeId != null ? attendeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attendee)) {
            return false;
        }
        Attendee other = (Attendee) object;
        if ((this.attendeeId == null && other.attendeeId != null) || (this.attendeeId != null && !this.attendeeId.equals(other.attendeeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.prakriti.socialtime.model.Attendee[ attendeeId=" + attendeeId + " ]";
    }
    
}
