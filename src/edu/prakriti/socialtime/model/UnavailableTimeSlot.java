/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.prakriti.socialtime.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PrAkRiTi
 */
@Entity
@Table(name = "unavailable_time_slot", catalog = "event_controller", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnavailableTimeSlot.findAll", query = "SELECT u FROM UnavailableTimeSlot u"),
    @NamedQuery(name = "UnavailableTimeSlot.findByUnTimeid", query = "SELECT u FROM UnavailableTimeSlot u WHERE u.unTimeid = :unTimeid"),
    @NamedQuery(name = "UnavailableTimeSlot.findByUserId", query = "SELECT u FROM UnavailableTimeSlot u WHERE u.userId = :userId"),
    @NamedQuery(name = "UnavailableTimeSlot.findByUnTime", query = "SELECT u FROM UnavailableTimeSlot u WHERE u.unTime = :unTime"),
    @NamedQuery(name = "UnavailableTimeSlot.findByUnDay", query = "SELECT u FROM UnavailableTimeSlot u WHERE u.unDay = :unDay"),
    @NamedQuery(name = "UnavailableTimeSlot.findByUnDate", query = "SELECT u FROM UnavailableTimeSlot u WHERE u.unDate = :unDate")})
public class UnavailableTimeSlot implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "un_timeid")
    private Integer unTimeid;
    @Basic(optional = false)
    @Column(name = "user_id")
    private int userId;
    @Basic(optional = false)
    @Column(name = "un_time")
    private String unTime;
    @Basic(optional = false)
    @Column(name = "un_day")
    private String unDay;
    @Basic(optional = false)
    @Column(name = "un_date")
    private String unDate;

    public UnavailableTimeSlot() {
    }

    public UnavailableTimeSlot(Integer unTimeid) {
        this.unTimeid = unTimeid;
    }

    public UnavailableTimeSlot(Integer unTimeid, int userId, String unTime, String unDay, String unDate) {
        this.unTimeid = unTimeid;
        this.userId = userId;
        this.unTime = unTime;
        this.unDay = unDay;
        this.unDate = unDate;
    }

    public Integer getUnTimeid() {
        return unTimeid;
    }

    public void setUnTimeid(Integer unTimeid) {
        this.unTimeid = unTimeid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUnTime() {
        return unTime;
    }

    public void setUnTime(String unTime) {
        this.unTime = unTime;
    }

    public String getUnDay() {
        return unDay;
    }

    public void setUnDay(String unDay) {
        this.unDay = unDay;
    }

    public String getUnDate() {
        return unDate;
    }

    public void setUnDate(String unDate) {
        this.unDate = unDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (unTimeid != null ? unTimeid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnavailableTimeSlot)) {
            return false;
        }
        UnavailableTimeSlot other = (UnavailableTimeSlot) object;
        if ((this.unTimeid == null && other.unTimeid != null) || (this.unTimeid != null && !this.unTimeid.equals(other.unTimeid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.prakriti.socialtime.model.UnavailableTimeSlot[ unTimeid=" + unTimeid + " ]";
    }
    
}
