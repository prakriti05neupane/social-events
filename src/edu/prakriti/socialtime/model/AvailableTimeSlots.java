/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.prakriti.socialtime.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PrAkRiTi
 */
@Entity
@Table(name = "available_time_slots", catalog = "event_controller", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AvailableTimeSlots.findAll", query = "SELECT a FROM AvailableTimeSlots a"),
    @NamedQuery(name = "AvailableTimeSlots.findByAvTimeId", query = "SELECT a FROM AvailableTimeSlots a WHERE a.avTimeId = :avTimeId"),
    @NamedQuery(name = "AvailableTimeSlots.findByAvTime", query = "SELECT a FROM AvailableTimeSlots a WHERE a.avTime = :avTime"),
    @NamedQuery(name = "AvailableTimeSlots.findByAvDay", query = "SELECT a FROM AvailableTimeSlots a WHERE a.avDay = :avDay"),
    @NamedQuery(name = "AvailableTimeSlots.findByAvDate", query = "SELECT a FROM AvailableTimeSlots a WHERE a.avDate = :avDate"),
    @NamedQuery(name = "AvailableTimeSlots.findByUserId", query = "SELECT a FROM AvailableTimeSlots a WHERE a.userId = :userId")})
public class AvailableTimeSlots implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "av_time_id")
    private Integer avTimeId;
    @Basic(optional = false)
    @Column(name = "av_time")
    private String avTime;
    @Basic(optional = false)
    @Column(name = "av_day")
    private String avDay;
    @Basic(optional = false)
    @Column(name = "av_date")
    private String avDate;
    @Basic(optional = false)
    @Column(name = "user_id")
    private int userId;

    public AvailableTimeSlots() {
    }

    public AvailableTimeSlots(Integer avTimeId) {
        this.avTimeId = avTimeId;
    }

    public AvailableTimeSlots(Integer avTimeId, String avTime, String avDay, String avDate, int userId) {
        this.avTimeId = avTimeId;
        this.avTime = avTime;
        this.avDay = avDay;
        this.avDate = avDate;
        this.userId = userId;
    }

    public Integer getAvTimeId() {
        return avTimeId;
    }

    public void setAvTimeId(Integer avTimeId) {
        this.avTimeId = avTimeId;
    }

    public String getAvTime() {
        return avTime;
    }

    public void setAvTime(String avTime) {
        this.avTime = avTime;
    }

    public String getAvDay() {
        return avDay;
    }

    public void setAvDay(String avDay) {
        this.avDay = avDay;
    }

    public String getAvDate() {
        return avDate;
    }

    public void setAvDate(String avDate) {
        this.avDate = avDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (avTimeId != null ? avTimeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AvailableTimeSlots)) {
            return false;
        }
        AvailableTimeSlots other = (AvailableTimeSlots) object;
        if ((this.avTimeId == null && other.avTimeId != null) || (this.avTimeId != null && !this.avTimeId.equals(other.avTimeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.prakriti.socialtime.model.AvailableTimeSlots[ avTimeId=" + avTimeId + " ]";
    }
    
}
