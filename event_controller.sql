-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2017 at 06:56 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `event_controller`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendee`
--

CREATE TABLE `attendee` (
  `attendee_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `event_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `available_time_slots`
--

CREATE TABLE `available_time_slots` (
  `av_time_id` int(11) NOT NULL,
  `av_time` varchar(50) NOT NULL,
  `av_day` varchar(50) NOT NULL,
  `av_date` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `available_time_slots`
--

INSERT INTO `available_time_slots` (`av_time_id`, `av_time`, `av_day`, `av_date`, `user_id`) VALUES
(1, '1`25', 'Sunday', '1`25', 3),
(2, '2 p.m- 5 p.m', 'Sunday', '2017- 4- 23', 3);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `duration` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `Description` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `user_id`, `event_name`, `duration`, `location`, `Description`) VALUES
(2, 1, 'get together', '2017-4-15 2p.m- 5p.m', 'koteshwor', 'Family get together'),
(4, 3, 'birthday party', '4 hours', 'ktm', 'birthday celebration'),
(5, 4, 'wedding', '3 hours', 'Baneshwor', 'skfd.kjdfk'),
(6, 3, 'meeting', '2 hours', 'Kapan', 'short meeting on children education'),
(7, 7, 'Seminar', '2017- 4- 18 11 a.m- 4 p.m', 'Soltee hotel', 'Short seminar on technical issues'),
(8, 3, 'Farewell', '2017- 4- 23  11 a.m- 4 p.m', 'Indreni banquet', 'college final students farewell'),
(9, 3, 'Engagement', '5- 8 p.m', 'Rubsana', 'jfjkdhiur'),
(10, 3, 'birthday', '5p.m', 'ktm', 'hjbmn'),
(11, 3, 'picnic', '11- 5p.m', 'kakani', 'daskfnkn,m'),
(12, 3, 'workshop', '11- 6', 'college', 'bjhjklkjl');

-- --------------------------------------------------------

--
-- Table structure for table `unavailable_time_slot`
--

CREATE TABLE `unavailable_time_slot` (
  `un_timeid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `un_time` varchar(50) NOT NULL,
  `un_day` varchar(50) NOT NULL,
  `un_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unavailable_time_slot`
--

INSERT INTO `unavailable_time_slot` (`un_timeid`, `user_id`, `un_time`, `un_day`, `un_date`) VALUES
(1, 3, '4254', 'Sunday', '1234'),
(2, 3, '8a.m- 1 p.m', 'Sunday', '2017- 4- 23');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(20) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `fullname` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `address` varchar(25) NOT NULL,
  `phone_no` varchar(25) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `event_request` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `fullname`, `email`, `address`, `phone_no`, `gender`, `event_request`) VALUES
(1, '', '', '', '', '', '', 'Male', 3),
(2, 'pracriti', '123', 'Prakriti neupane', 'pracriti11@gmail.com', 'Bhaktapur', '897386478', 'Female', 3),
(3, 'abc', '123', 'ABC', 'abc@gmail.com', 'xyz', '65765674', 'Male', 0),
(4, 'aastha', 'gautam', 'AasthaGautam', 'aastha@gmail.com', 'changu', '8765378', 'Female', 3),
(7, 'uihuihui', 'djhgsdaj', 'jkafhjk', 'jkda@gmail.com', 'kdljsafkl', '78342678532', 'Male', 3),
(8, 'prakriti_neupane', 'pkt123', 'Prakriti Neupane', 'prakriti25@gmail.com', 'Thimi', '09897897', 'Female', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendee`
--
ALTER TABLE `attendee`
  ADD PRIMARY KEY (`attendee_id`);

--
-- Indexes for table `available_time_slots`
--
ALTER TABLE `available_time_slots`
  ADD PRIMARY KEY (`av_time_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `unavailable_time_slot`
--
ALTER TABLE `unavailable_time_slot`
  ADD PRIMARY KEY (`un_timeid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendee`
--
ALTER TABLE `attendee`
  MODIFY `attendee_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `available_time_slots`
--
ALTER TABLE `available_time_slots`
  MODIFY `av_time_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `unavailable_time_slot`
--
ALTER TABLE `unavailable_time_slot`
  MODIFY `un_timeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
