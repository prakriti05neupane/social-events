package edu.prakriti.socialtime.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2017-04-23T07:49:35")
@StaticMetamodel(Event.class)
public class Event_ { 

    public static volatile SingularAttribute<Event, String> duration;
    public static volatile SingularAttribute<Event, Integer> eventId;
    public static volatile SingularAttribute<Event, String> location;
    public static volatile SingularAttribute<Event, String> description;
    public static volatile SingularAttribute<Event, Integer> userId;
    public static volatile SingularAttribute<Event, String> eventName;

}