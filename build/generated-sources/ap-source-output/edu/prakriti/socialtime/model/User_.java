package edu.prakriti.socialtime.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2017-04-23T07:49:35")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, String> phoneNo;
    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, String> address;
    public static volatile SingularAttribute<User, Integer> userId;
    public static volatile SingularAttribute<User, String> gender;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, Integer> eventRequest;
    public static volatile SingularAttribute<User, String> fullname;

}