package edu.prakriti.socialtime.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2017-04-23T07:49:35")
@StaticMetamodel(AvailableTimeSlots.class)
public class AvailableTimeSlots_ { 

    public static volatile SingularAttribute<AvailableTimeSlots, String> avTime;
    public static volatile SingularAttribute<AvailableTimeSlots, String> avDay;
    public static volatile SingularAttribute<AvailableTimeSlots, Integer> avTimeId;
    public static volatile SingularAttribute<AvailableTimeSlots, Integer> userId;
    public static volatile SingularAttribute<AvailableTimeSlots, String> avDate;

}