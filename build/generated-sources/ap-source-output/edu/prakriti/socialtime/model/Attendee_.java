package edu.prakriti.socialtime.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2017-04-23T07:49:35")
@StaticMetamodel(Attendee.class)
public class Attendee_ { 

    public static volatile SingularAttribute<Attendee, Integer> attendeeId;
    public static volatile SingularAttribute<Attendee, Integer> eventId;
    public static volatile SingularAttribute<Attendee, Integer> userId;

}