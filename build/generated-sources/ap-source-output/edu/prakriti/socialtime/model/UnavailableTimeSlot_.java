package edu.prakriti.socialtime.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2017-04-23T07:49:35")
@StaticMetamodel(UnavailableTimeSlot.class)
public class UnavailableTimeSlot_ { 

    public static volatile SingularAttribute<UnavailableTimeSlot, Integer> unTimeid;
    public static volatile SingularAttribute<UnavailableTimeSlot, String> unDate;
    public static volatile SingularAttribute<UnavailableTimeSlot, String> unTime;
    public static volatile SingularAttribute<UnavailableTimeSlot, String> unDay;
    public static volatile SingularAttribute<UnavailableTimeSlot, Integer> userId;

}