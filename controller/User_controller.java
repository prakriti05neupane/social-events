/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ksitiz.socialtime.controller;

import edu.ksitiz.socialtime.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kxitiz
 */
public class User_controller {
    
private Connection connection = null;
    private PreparedStatement projmt;
    private ResultSet rs;
    

    
    
    public User_controller(){
        if(connection == null){
            connection = DBConnection.getConnection();
        }
    }
    
    public User authenticate(User userObj){
       User currentUser = null;
       
       String query = "select * from user where email = ? and password = ?";
       
       try{
          projmt = connection.prepareStatement(query);
          projmt.setString(1, userObj.getEmail());
          projmt.setString(2, userObj.getPassword());
          
          rs = projmt.executeQuery();
          
          while (rs.next()){
              currentUser = new User();
              currentUser.setUserId(rs.getInt("user_id"));
              currentUser.setEmail(rs.getString("email"));
          }
       } catch (SQLException ex) {
            Logger.getLogger(User_controller.class.getName()).log(Level.SEVERE, null, ex);
        }
       return currentUser;
    }
    public int register(User userObj){
        int isUserRegistered = 0;
        String query = "INSERT into User(email, password, Fullname, Gender, skill) values(?,?,?,?,?)";
        try{
            projmt= connection.prepareStatement(query);
            projmt.setString(1, userObj.getUserName());
            projmt.setString(2, userObj.getEmail());
            projmt.setString(3, userObj.getAddress());
            projmt.setString(4, userObj.getFullName());
            projmt.setString(5,userObj.getPassword());
            projmt.setString(6,userObj.getRePassword());
            
            isUserRegistered= projmt.executeUpdate();
        }catch(SQLException e){
            Logger.getLogger(User_controller.class.getName()).log(Level.SEVERE,null,e);
            
        }
        
        return isUserRegistered;
    }    
}

