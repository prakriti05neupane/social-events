package edu.ksitiz.socialtime.controller;

import edu.ksitiz.socialtime.view.LoginForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginControler implements ActionListener{

    LoginForm loginform;
    
    public LoginControler(){
        loginform=new LoginForm();
        loginform.btnActn(this);
    }
    
    public void showProfile(){
        loginform.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==loginform.getBtnLogin()) {
            new ProfileController();
        }else if (e.getSource()==loginform.getBtnRegister()) {
            new RegisterController();
        }
    }
}
