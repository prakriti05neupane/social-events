package edu.ksitiz.socialtime.controller;

import edu.ksitiz.socialtime.view.ProfileRegisteredUser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProfileController implements ActionListener{
    
    ProfileRegisteredUser pru;
    
    public ProfileController(){
        pru=new ProfileRegisteredUser();
        pru.btnActn(this);
        showGUI();
    }
    
    public void showGUI(){
        pru.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==pru.getBtnEventCreate()) {
            new EventFormController();
        }
    }
}
