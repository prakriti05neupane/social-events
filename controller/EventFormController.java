package edu.ksitiz.socialtime.controller;

import edu.ksitiz.socialtime.view.EventForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EventFormController implements ActionListener{

    EventForm evtform;
    
    public EventFormController(){
        evtform=new EventForm();
        showGUI();
    }
    
    public void showGUI(){
        evtform.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
    
}
