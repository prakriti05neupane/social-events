package edu.ksitiz.socialtime.controller;

import edu.ksitiz.socialtime.view.RegistrationForm;

public class RegisterController {
    
    RegistrationForm reform;
    
    public RegisterController(){
        reform=new RegistrationForm();
        showGUI();
    }
    
    public void showGUI(){
        reform.setVisible(true);
    }
}
